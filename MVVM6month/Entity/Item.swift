//
//  Item.swift
//  MVVM6month
//
//  Created by Samir Maksutov on 18/6/22.
//

import Foundation

struct Item {
    let name: String
    let age: Int
}
