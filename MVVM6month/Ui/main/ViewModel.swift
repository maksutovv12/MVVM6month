//
//  ViewModel.swift
//  MVVM6month
//
//  Created by Samir Maksutov on 18/6/22.
//

import Foundation
import UIKit

class ViewModel {
    
   private let back = Backend()
   var item = Dynamic(Item(name: String(), age: Int()))
    
    init() {
        
    }
    
    func getData(){
        item.value = back.getItemDFromBack()
    }
    
}
