//
//  ViewController.swift
//  MVVM6month
//
//  Created by Samir Maksutov on 18/6/22.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    private let viewModel = ViewModel()
    
    private lazy var tittleView: UILabel = {
        let view = UILabel()
        view.text = "Example"
        return view
    }()
    
    private lazy var btn: UIButton = {
       let view = UIButton()
        view.setTitle("start", for: .normal)
        view.backgroundColor = .blue
        view.addTarget(self, action: #selector(tapBtn(_:)), for: .touchUpInside)
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        setupSubviews()
        setupView()
    }
    
    private func bindViewModel(){
        viewModel.item.bind{ Item in
            DispatchQueue.main.async {
                self.tittleView.text = Item.name
            }
        }
    }
    
    private func setupView(){
        
    }
    
    @objc func tapBtn(_ ui: UIButton){
        viewModel.getData()
    }
    
    private func setupSubviews(){
        view.addSubview(tittleView)
        tittleView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        view.addSubview(btn)
        btn.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
        }
    }
}

