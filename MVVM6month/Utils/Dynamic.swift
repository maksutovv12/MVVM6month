//
//  Dynamic.swift
//  MVVM6month
//
//  Created by Samir Maksutov on 18/6/22.
//

import Foundation

class Dynamic <T> {
    
    typealias Listner = (T) -> Void
    private var listener: Listner?
    
    func bind(_ listener: Listner?){
        self.listener = listener
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init (_ v:T){
        value = v
    }
    
}
